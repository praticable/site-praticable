# Première version stable

On ne dirait pas mais le code de Praticable est presque entièrement neuf, optimisé et plus stable. Cette nouvelle version apporte également quelques nouvelles fonctionnalités. On peut considérer la version précédente comme un prototype et celle-ci comme la première version stable.

## Raccourcis clavier

### Lors de l'édition d'un bloc texte

- Touches `ctrl ou cmd + b` pour appliquer **gras** à la sélection.
- Touches `ctrl ou cmd + i` pour appliquer _italic_ à la sélection.
- Touches `ctrl ou cmd + k` pour créer un lien : [lien](exemple.fr).
- Touche `échap` pour annuler l'édition.
- Touches `ctrl ou cmd + entrée` pour confirmer l'édition.

### Autres

- Touches `ctrl ou cmd + a` pour sélectionner tous les blocs en même temps (voire plus bas).

The register button is displayed when the interface is not up to date, specifically when the `isUpToDate` state variable is set to `false`. Previously, any modification to the interface required a manual update of the `isUpToDate` state. Now, the `isUpToDate` state dynamically computes a deep comparison between the registerable properties of the local version and the server blocks.

## Édition multiple

Plusieurs blocs peuvent maintenant être supprimés, dupliqués, déplacés et redimensionnés en même temps. Maitenez la touche `cmd ou ctrl` enfoncé et cliquez sur un bloc pour l'ajouter à la sélection. Appuyez simultanément sur les touches `ctrl ou cmd + a` pour sélectionner tous les blocs.

## Édition des légendes d'image en front

Auparavant éditables via le panel de Kirby, les légendes d'images le sont désormais depuis le front. Lorsque vous êtes connecté, sélectionnez un bloc image pour éditer sa légende.

## Améliorations diverses

- Autofocus du champ d'édition
- Plus de rechargement de page après ajout d'un bloc image
- Le menu contextuel est désormais accessible via un simple clic-droit (plutôt que `cmd).

## Technologies

On passe de Kirby 3 à [Kirby 4](https://getkirby.com/releases/4.0).
On passe de Vue.js 2 à Vue.js 3 et de l'[API options](https://guide.vueframework.com/api/options-api.html) à l'[API composition](https://vuejs.org/guide/extras/composition-api-faq.html), avec [Pinia.js](https://pinia.vuejs.org/), désormais store officiel de Vue.js. En conséquence, le code est globalement bien mieux organisé, notamment côté store puisque qu'on passe d'un fichier de près de 1 000 lignes à une quinzaine de petits fichiers séparés.

## Approche plugin

Après avoir essayé différentes approches
