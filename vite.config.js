import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: '/',
  server: {
    port: 5173,
  },
  build: {
    minify: false,
    rollupOptions: {
      output: {
        entryFileNames: 'assets/dist/[name].js',
        assetFileNames: 'assets/dist/[name].css',
      },
      external: [],
    },
    commonjsOptions: {
      transformMixedEsModules: true,
    },
  },
});
