title: Espace
icon: sitemap

tabs:
  contentTab:
    label: Contenu
    icon: dashboard
    columns:
      - width: 1/3
        sections:
          fieldSection:
            type: fields
            fields:
              isExternal:
                label: Lien externe ?
                type: toggle
              externalUrl:
                label: URL
                type: url
                when:
                  isExternal: true
              tags:
                label: Mots-clés
                type: multiselect
                options: query
                query: site.index.filterBy('template', 'tag')
                when:
                  isExternal: false
              mode:
                type: select
                options:
                  iframe: iframe
                  composition: Composition
                  htmlFiles: Fichiers HTML zippés
                  pdf: PDF
                default: composition
                when:
                  isExternal: false
              linkedSpaces:
                label: Pages liées
                type: pages
                query: site.index.filterBy('template', 'space')
                limit: 10
                when:
                  isExternal: false
          childrenSection:
            headline: Sous-pages
            type: pages
            template: space
            create: space
            search: true
            when:
              isExternal: false

      - width: 2/3
        sections:
          compositionSection:
            type: fields
            fields:
              composition:
                label: Blocs
                type: blocks
                help: Générés depuis le front ou à la création de sous-pages (onglet sous-pages ↑).
                fieldsets:
                  - markdown
                  - image
                  - representative
                when:
                  isExternal: false

  configTab:
    label: config
    icon: settings
    sections:
      configSection:
        type: fields
        fields:
          layout:
            label: Mise en écran par défault
            type: select
            options:
              free: Libre
              vertical: Verticale
              horizontal: Horizontale
              full: Diaporama
            default: free
            width: 1/3
          disableFreeLayout:
            label: Désactiver la mise en écran libre
            type: toggle
            default: false
            width: 1/3
          isOpenSpace:
            label: Espace ouvert
            type: toggle
            width: 1/3
          disableRepresentative:
            label: Désactiver représentant
            type: toggle
            width: 1/3
          isDesktopOnly:
            label: Cacher sur mobile
            type: toggle
            width: 1/3
            default: false

  filesTab:
    label: Fichiers
    icon: images
    columns:
      - width: 1/3
        sections:
          filesInfoSection:
            headline: Info
            type: info
            text: |
              Supprimez les fichiers inutilisés pour ne pas surcharger inutilement le serveur.

      - width: 2/3
        sections:
          filesSection:
            headline: Fichiers
            type: files
            layout: cards

  apisTab:
    label: API(s)
    icon: refresh
    sections:
      apisSection:
        label: API(s)
        type: fields
        fields:
          distantId:
            label: ID distant
            type: text
            placeholder: Optionnel
          connections:
            type: structure
            fields:
              api:
                label: API(s)
                type: select
                options: query
                query:
                  fetch: site.apis.toStructure
                  text: "{{ structureItem.name }}"
                  value: "{{ structureItem.name }} - {{ structureItem.tool }}"
                width: 1/3
              id:
                label: ID distant
                type: text
                help: |
                  https://collectifbam.notion.site/Principes-de-design- **[4afffea43a6a424ab864daaceae81d41]**
                width: 1/3
                whenQuery: api =~ 'notion'
              target:
                label: Cible
                type: select
                width: 1/3
                options:
                  block: Bloc
                  database: Base de données
                  page: Page
                  subpages: Page et sous-pages
                whenQuery: api =~ 'notion'
          update:
            type: fetchAPI
            width: 1/4

  seoTab:
    label: SEO
    icon: search
    sections:
      fields:
        fields:
          seoTitle:
            label: Titre SEO
            type: text
            icon: headline
            help: Permet de fournir aux moteurs de recherche un titre différent de celui affiché sur la page.
            width: 1/2
          description:
            label: Meta description
            type: text
            icon: text
            maxlength: 155
            help: Utilisée par les moteurs de recherche. Sur mobile, les moteurs n'affichent que 120 caractères, 155 sur ordinateur.
            width: 1/2
          author:
            label: Auteur(s)·rice(s)
            type: text
            icon: user
            width: 1/2
          published:
            label: Date de publication
            type: date
            display: DD/MM/YYYY
            default: today
            width: 1/2
