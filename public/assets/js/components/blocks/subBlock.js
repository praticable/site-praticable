import blockMixin from "./block-mixin.js";
import BlockText from "./block-text.js";
import BlockImage from "./block-image.js";
import Store from "../../store.js";
import BtnsMd from "../buttons/btns-md.js";

const SubBlock = {
  mixins: [blockMixin],
  props: {
    block: Object,
    page: String,
  },
  components: {
    "block-text": BlockText,
    "block-image": BlockImage,
    "btns-md": BtnsMd,
  },
  data: function () {
    return {
      store: Store,
      heightTextarea: "",
      initialZIndex: this.block.content.zindex,
      isHover: false,
    };
  },
  computed: {
    isHighlight: function () {
      return this.block.isHighlight || false;
    },
  },
  template: `
    <div v-if="block.type === 'text' && isEdit">
      <btns-md
        :block="block"
        :style="
            'transform: ' + block.content.transform + ' scale(0.666); ' +
            'z-index: 998'
        "
      ></btns-md>

        <textarea 
            v-if="block.type === 'text' && isEdit"
            v-model="block.content.text"
            
            @input="autoExpand"
            
            ref="textarea"
            :style="
                'width: ' + block.content.width + '; ' +
                'height: ' + block.content.height + '; ' +
                'transform: ' + block.content.transform + '; ' +
                'z-index: 998'
            " 
            class="block--text autoExpand" 
            placeholder="Entrez votre texte"
        />
      </div>
      <div 
          v-else
          class="block white" 
          :class="[
              'block--' + block.type,
              { edit: isEdit },
              { flex: block.type === 'options' },
              { highlight: block.isHighlight }
          ]" 
          :data-type="block.type" 
          :style="
              'width: ' + block.content.width + '; ' +
              'height: ' + block.content.height + '; ' +
              'transform: ' + block.content.transform + '; ' +
              'z-index: ' + block.content.zindex + ';' +
              'background-color: ' + block.content.backgroundcolor + ';' +
              'color :' + block.content.textcolor + ';'
          "

          @dblclick="edit" 
          @click="select"
          @mouseup="saveStyle"
          @mouseenter="mouseEnter"
          @mouseleave="mouseLeave"
          
          ref="block">
          <component 
            :is="'block-' + block.type" 
            :block="block" 
            :isEdit="isEdit" 
            :page="page"
            :update-signal="updateSignal"
          ></component>
      </div>
    </div>
    `,
  methods: {
    getScrollHeight: function (el) {
      var savedValue = el.value;
      el.value = "";
      el._baseScrollHeight = el.scrollHeight;
      el.value = savedValue;
    },
    autoExpand: function (el) {
      const target = el.target;
      target.style.height = target.scrollHeight + "px";
      this.block.content.height = target.style.height;
    },
    mouseEnter: function () {
      this.toFirstPlan();
      if (
        (this.block.type === "subpage" ||
          this.block.type === "subpage-cover") &&
        this.block.content.ref.length > 0
      ) {
        this.store.highlightBlockByTitle(this.block.content.ref);
        this.store.highlightBlockById(this.block.id);
      }
    },
    mouseLeave: function () {
      this.toInitialPlan();
      if (
        (this.block.type === "subpage" ||
          this.block.type === "subpage-cover") &&
        this.block.content.ref.length > 0
      ) {
        this.store.unhighlightBlockByTitle(this.block.content.ref);
        this.store.unhighlightBlockById(this.block.id);
      }
    },
    toFirstPlan: function () {
      this.isHover = true;
      this.$refs.block.style.zIndex = this.$root.maxZIndex + 1;
      if (this.$root.debug) {
        console.log(this.$options._componentTag + " toFirstPlan");
      }
    },
    toInitialPlan: function () {
      this.isHover = false;
      this.$refs.block.style.zIndex = this.block.content.zindex;
      if (this.$root.debug) {
        console.log(this.$options._componentTag + " toInitialPlan");
      }
    },
  },
};

export default SubBlock;
