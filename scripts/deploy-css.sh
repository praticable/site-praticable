npm install praticable-css
rm -rf ./src/assets/css/praticable-css
cp -r ./node_modules/praticable-css ./src/assets/css/praticable-css
if ! grep -q '@import "assets/css/praticable-css/style.css";' src/style.css; then
    awk 'BEGIN {print "@import \"assets/css/praticable-css/style.css\";"} 1' src/style.css > temp && mv temp src/style.css
fi