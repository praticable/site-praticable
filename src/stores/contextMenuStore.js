import { defineStore } from "pinia"
import { ref } from "vue"

export const useContextMenuStore = defineStore("contextMenuStore", () => {
  const target = ref(null)
  const isOpen = ref(false)
  const position = ref({
    left: null,
    top: null,
  })

  // methods
  function openContextMenu(event, clickedTarget) {
    event.preventDefault()
    target.value = clickedTarget
    isOpen.value = true
    position.value.left = event.clientX - 30
    position.value.top = event.clientY - 30
  }

  return { openContextMenu, isOpen, position, target }
})
