import { defineStore, storeToRefs } from "pinia"
import { useViewStore } from "../viewStore.js"
import { ref } from "vue"

export const useMobileIndexStore = defineStore("mobileIndexStore", () => {
  // state
  const currentSection = ref("")
  const isOpen = ref(false)
  // stores
  const { isDesktop } = storeToRefs(useViewStore())
  //methods
  function navTo(section, event) {
    if (isDesktop.value) return
    event.preventDefault()

    currentSection.value = section

    const newActiveSection = event.currentTarget.closest("li")
    newActiveSection.classList.add("active-section")

    this.activeParents(newActiveSection)
    this.sizeDisplayedUl()
  }
  function activeParents(activeSection) {
    const parent =
      activeSection.tagName === "UL"
        ? activeSection.closest("li")
        : activeSection.closest("ul")
    if (!parent.classList.contains("active-section"))
      parent.classList.add("active-section")

    if (parent.classList.contains("lock-visible")) return
    this.activeParents(parent)
  }
  function sizeDisplayedUl() {
    const activeSections = document.querySelectorAll(".active-section")

    this.removeCurrentOptionsClass()

    const target = activeSections[activeSections.length - 1]
      ? activeSections[activeSections.length - 1].querySelector("ul")
      : document.querySelector(".lock-visible")

    const optionsMaxHeight = this.calculateOptionsMaxHeight()
    if (target.offsetHeight > optionsMaxHeight) {
      target.classList.add("scroll-y")
    }
  }
  function removeCurrentOptionsClass() {
    if (document.querySelector(".scroll-y")) {
      document.querySelector(".scroll-y").classList.remove("scroll-y")
    }
  }
  function calculateOptionsMaxHeight() {
    if (!document.querySelector("#viewfinder--mobile__current-section")) return
    const btnHeight = document.querySelector(
      "#viewfinder--mobile__current-section"
    ).offsetHeight
    const optionsMaxHeight = window.innerHeight - btnHeight

    return optionsMaxHeight
  }
  return { navTo, currentSection, isOpen }
})
