import { defineStore } from "pinia"

export const useViewfinderBtnStore = defineStore("viewfinderBtnStore", () => {
  function setHoverColor(event) {
    if (!event.target.closest(".wrapper")) return
    event.target.closest(".wrapper").classList.add("hover")
  }

  function unsetHoverColor(event) {
    if (!event.target.closest(".wrapper")) return
    event.target.closest(".wrapper").classList.remove("hover")
  }

  return { setHoverColor, unsetHoverColor }
})
