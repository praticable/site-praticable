import { defineStore } from "pinia"
import { ref } from "vue"

export const useIndexStore = defineStore("indexStore", () => {
  const index = ref([])
  const flatIndex = ref([])

  const currentSpaceName = ref("")
  const currentSpaceUri = ref("")
  const breadcrumb = ref([])

  const isOpen = ref(false)

  return {
    index,
    flatIndex,
    currentSpaceName,
    currentSpaceUri,
    breadcrumb,
    isOpen,
  }
})
