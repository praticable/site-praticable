import { defineStore, storeToRefs } from "pinia"
import { ref, computed, watch } from "vue"
import { useBlockStore } from "../blocks/blockStore.js"
import { useIndexStore } from "./indexStore.js"
import { useViewfinderBtnStore } from "./viewfinderBtnStore.js"
import { useMobileIndexStore } from "./mobileIndexStore.js"
import { useTuneStore } from "./tuneStore.js"
import { useModifyStore } from "./modifyStore.js"

export const useViewfinderStore = defineStore("viewfinderStore", () => {
  // child stores
  const indexStore = useIndexStore()
  const tuneStore = useTuneStore()
  const modifyStore = useModifyStore()
  const viewfinderBtnStore = useViewfinderBtnStore()
  const mobileIndexStore = useMobileIndexStore()

  //computed
  const isViewfinderActive = computed({
    get: () => {
      return (
        indexStore.isOpen ||
        mobileIndexStore.isOpen ||
        tuneStore.isOpen ||
        modifyStore.isOpen
      )
    },
    set: (bool) => {
      if (!bool) close()
    },
  })

  // methods
  function enableDragToScroll() {
    const ele = document.getElementById("app")
    ele.style.cursor = "grab"

    let pos = { top: 0, left: 0, x: 0, y: 0 }

    const mouseDownHandler = function (e) {
      const isVerticalMode = e.target.classList.contains("vertical")
      if (!e.target.classList.contains("dragscroll")) return
      document.querySelector("html").style.scrollBehavior = "auto"

      ele.style.cursor = "grabbing"
      ele.style.userSelect = "none"

      pos = {
        left: window.scrollX,
        top: window.scrollY,
        // Get the current mouse position
        x: e.clientX,
        y: e.clientY,
      }

      document.addEventListener("mousemove", mouseMoveHandler)
      document.addEventListener("mouseup", mouseUpHandler)
    }

    const mouseMoveHandler = function (e) {
      // How far the mouse has been moved
      const dx = e.clientX - pos.x
      const dy = e.clientY - pos.y

      // Scroll the element
      window.scrollTo(pos.left - dx, pos.top - dy)
    }

    const mouseUpHandler = function () {
      ele.style.cursor = "grab"
      ele.style.removeProperty("user-select")

      document.removeEventListener("mousemove", mouseMoveHandler)
      document.removeEventListener("mouseup", mouseUpHandler)

      document.querySelector("html").removeAttribute("style")
    }

    // Attach the handler
    ele.addEventListener("mousedown", mouseDownHandler)
  }
  function clearActiveItem() {
    const activeItems = document.querySelectorAll(".active-section")
    activeItems.forEach((activeItem) => {
      activeItem.classList.remove("active-section")
    })
  }
  function close() {
    indexStore.isOpen = false
    mobileIndexStore.isOpen = false
    tuneStore.isOpen = false
    modifyStore.isOpen = false
  }

  return {
    isViewfinderActive,
    indexStore,
    viewfinderBtnStore,
    mobileIndexStore,
    enableDragToScroll,
  }
})
