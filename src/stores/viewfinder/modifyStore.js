import { defineStore } from "pinia"
import { ref } from "vue"

export const useModifyStore = defineStore("modifyStore", () => {
  const isOpen = ref(false)
  return { isOpen }
})
