import { defineStore } from "pinia"
import { ref } from "vue"

export const useTuneStore = defineStore("tuneStore", () => {
  const isOpen = ref(false)

  return { isOpen }
})
