import { defineStore } from "pinia"

export const useUtilStore = defineStore("utilStore", () => {
  function debounce(func, wait = 10, immediate = true) {
    let timeout
    return function () {
      let context = this,
        args = arguments
      let later = function () {
        timeout = null
        if (!immediate) func.apply(context, args)
      }
      let callNow = immediate && !timeout
      clearTimeout(timeout)
      timeout = setTimeout(later, wait)
      if (callNow) func.apply(context, args)
    }
  }

  function capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  function addTemporaryClass(target, className, durationInMs) {
    target.classList.add(className)
    setTimeout(() => {
      target.classList.remove(className)
    }, durationInMs)
  }

  function getElementOffset(el) {
    const rect = el.getBoundingClientRect()
    return {
      left: rect.left + window.scrollX,
      top: rect.top + window.scrollY,
    }
  }

  return {
    debounce,
    capitalizeFirstLetter,
    addTemporaryClass,
    getElementOffset,
  }
})
