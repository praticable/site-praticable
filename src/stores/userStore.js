import { defineStore } from "pinia"
import { ref } from "vue"

export const useUserStore = defineStore("userStore", () => {
  const isLog = ref(null)
  return { isLog }
})
