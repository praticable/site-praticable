import { defineStore } from "pinia"
import { ref, computed, onMounted, watch } from "vue"
import { useBlockStore } from "./blocks/blockStore.js"

export const useViewStore = defineStore("viewStore", () => {
  const isDesktop = ref(null)
  const isMobile = computed(() => !isDesktop.value)
  const selectedLayout = ref("free")
  const slideIndex = ref(0)
  const isOpenSpace = ref(null)

  // stores
  const {
    getBlockRightEdgeX,
    getBlockBottomEdgeY,
    getRightMostBlock,
    getLowestBlock,
  } = useBlockStore()

  // watchers
  onMounted(() => {
    const appNode = document.querySelector("#app")
    const blocksNode = document.querySelector("#blocks")
    appNode.classList.add(selectedLayout.value)
    blocksNode.classList.add(selectedLayout.value)
    watch(selectedLayout, (newSelectedLayout) => {
      const layoutClass = appNode.classList[appNode.classList.length - 1]

      appNode.classList.remove(layoutClass)
      blocksNode.classList.remove(layoutClass)
      appNode.classList.add(newSelectedLayout)
      blocksNode.classList.add(newSelectedLayout)
    })
  })

  // methods
  function setDevice() {
    isDesktop.value = window.innerWidth > 800
  }
  function updateDevice() {
    isDesktop.value = window.innerWidth > 800
  }
  function setSpaceSize(mode = "end") {
    const app = document.querySelector("#app")

    const margin = 400
    const width = getBlockRightEdgeX(getRightMostBlock()) + margin + "px"
    const height = getBlockBottomEdgeY(getLowestBlock()) + margin + "px"

    const hasToReduce = app.style.width > width || app.style.height > height
    if (mode === "end" && hasToReduce) {
      app.style.transition = "width .5s ease-in-out, height .5s ease-in-out"
    }
    if (mode === "move") {
      if (app.style.width < width) app.style.width = width
      if (app.style.height < height) app.style.height = height
    } else {
      app.style.width = width
      app.style.height = height
    }

    setTimeout(() => {
      app.style.transition = ""
    }, 600)
  }

  return {
    isDesktop,
    isMobile,
    isOpenSpace,
    selectedLayout,
    slideIndex,
    setDevice,
    updateDevice,
    setSpaceSize,
  }
})
