import { defineStore, storeToRefs } from 'pinia';
import { useUserStore } from './userStore.js';
import { useViewStore } from './viewStore.js';
import { useIndexStore } from './viewfinder/indexStore.js';
import { useBlockStore } from './blocks/blockStore.js';
import { computed, onMounted, ref } from 'vue';

export const useSaveStore = defineStore('saveStore', () => {
  // data
  const isFeedbackMessageDisplayed = ref(false);

  // template refs
  const registerBtnNode = ref();

  // stores
  const { isLog } = storeToRefs(useUserStore());
  const { isOpenSpace } = storeToRefs(useViewStore());
  const { currentSpaceUri, currentSpaceName } = storeToRefs(useIndexStore());
  const {
    localBlocks,
    serverBlocks,
    localStaticBlocks,
    serverStaticBlocks,
    backupBlocks,
  } = storeToRefs(useBlockStore());

  // computed
  const isUpToDate = computed(() => {
    return (
      JSON.stringify(localStaticBlocks.value) ===
      JSON.stringify(serverStaticBlocks.value)
    );
  });

  // init functions
  confirmBeforeUnload();

  // methods
  async function saveSpace() {
    if (!isLog.value && !isOpenSpace.value) return;

    updateRegisterButtonContent('enregistrement…');

    const data = prepareData();

    const init = {
      method: 'POST',
      body: JSON.stringify(data),
    };

    try {
      const res = await sendRequest(`./save.json`, init);
      const json = await res.json();

      if (isSaveSuccessful(json, data)) {
        handleSuccessfullSave(json);
      } else {
        handleFailedSave(json);
      }
    } catch (error) {
      handleFetchError(error);
    }
  }

  function updateRegisterButtonContent(content) {
    if (!registerBtnNode.value) return;
    registerBtnNode.value.textContent = content;
  }

  function prepareData() {
    return {
      spaceUri: currentSpaceUri.value,
      blocks: localBlocks.value,
    };
  }

  async function sendRequest(url, init) {
    return fetch(url, init);
  }

  function isSaveSuccessful(json, data) {
    return (
      json.status !== 'error' &&
      json.blocks &&
      json.blocks.length === data.blocks.length
    );
  }

  function handleSuccessfullSave(json) {
    console.log(
      'Enregistrement réussi. Nouvelles données de la composition :',
      json
    );
    isFeedbackMessageDisplayed.value = true;
    setTimeout(() => {
      isFeedbackMessageDisplayed.value = false;
    }, 2000);
    registerBtnNode.value.classList.add('active');
    registerBtnNode.value.textContent = 'enregistré';
    localUpdate();
  }

  function localUpdate() {
    serverBlocks.value = JSON.parse(JSON.stringify(localBlocks.value));
  }

  function handleFailedSave(json) {
    console.log(
      "L'enregistrement a échoué. Pour aider à résoudre le problème, envoyez au développeur une capture d'écran complète avec l'erreur suivante : ",
      json
    );
    isFeedbackMessageDisplayed.value = true;
    setTimeout(() => {
      isFeedbackMessageDisplayed.value = false;
    }, 2000);
    registerBtnNode.value.textContent =
      "Erreur. Ouvrez la console (F12) pour plus d'infos.";
    registerBtnNode.value.classList.add('warn');
    return Promise.reject(json);
  }

  function handleFetchError(error) {
    console.error("Une erreur s'est produite lors de la requête :", error);
  }

  function confirmBeforeUnload() {
    window.onbeforeunload = function (e) {
      if (!isUpToDate.value) {
        e = e || window.event;

        // For IE and Firefox prior to version 4
        if (e) {
          e.returnValue =
            'Voulez-vous vraiment quitter la page ? Les modifications non enregistrées seront perdues.';
        }

        // For Safari
        return 'Voulez-vous vraiment quitter la page ? Les modifications non enregistrées seront perdues.';
      }
    };
  }

  return {
    isFeedbackMessageDisplayed,
    registerBtnNode,
    isUpToDate,
    saveSpace,
    localUpdate,
  };
});
