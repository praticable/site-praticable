import { defineStore } from 'pinia';

export const useBlockPositionStore = defineStore('blockPositionStore', () => {
  function getRightMostBlock() {
    const blockNodesArray = Array.prototype.slice.call(
      document.querySelectorAll('.block')
    );
    const sortedBlockNodes = blockNodesArray.sort((blockA, blockB) => {
      const blockARightPosition = getBlockRightEdgeX(blockA);
      const blockBRightPosition = getBlockRightEdgeX(blockB);
      return blockBRightPosition - blockARightPosition;
    });
    const rightMostBlock = sortedBlockNodes[0];
    return rightMostBlock;
  }

  function getBlockRightEdgeX(blockNode) {
    const { x } = parseTransformForBlockCoordinates(blockNode.style.transform);
    const width = parseInt(
      window.getComputedStyle(blockNode).width.replace('px', '')
    );
    const rightEdgeX = x + width;
    return rightEdgeX;
  }

  function getBlockBottomEdgeY(blockNode) {
    const { y } = parseTransformForBlockCoordinates(blockNode.style.transform);
    const height = parseInt(
      window.getComputedStyle(blockNode).height.replace('px', '')
    );
    const bottomEdgeY = y + height;
    return bottomEdgeY;
  }

  function getLowestBlock() {
    const blockNodesArray = Array.prototype.slice.call(
      document.querySelectorAll('.block')
    );
    const sortedBlockNodes = blockNodesArray.sort((blockA, blockB) => {
      const blockABottomPosition = getBlockBottomEdgeY(blockA);
      const blockBBottomPosition = getBlockBottomEdgeY(blockB);
      return blockBBottomPosition - blockABottomPosition;
    });
    const lowestBlock = sortedBlockNodes[0];
    return lowestBlock;
  }

  function parseTransformForBlockCoordinates(transform) {
    if (transform.length === 0)
      return {
        x: 0,
        y: 0,
      };
    const position = [...transform.matchAll(/-?\d+(\.\d+)?/g)];
    return {
      x: parseFloat(position[0]),
      y: parseFloat(position[1] || 0),
    };
  }

  return {
    getRightMostBlock,
    getLowestBlock,
    getBlockRightEdgeX,
    getBlockBottomEdgeY,
    parseTransformForBlockCoordinates,
  };
});
