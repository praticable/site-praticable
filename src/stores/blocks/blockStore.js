import { defineStore, storeToRefs } from 'pinia';
import { ref, computed, watch, onMounted } from 'vue';
import { useIndexStore } from '../viewfinder/indexStore.js';
import { useViewfinderStore } from '../viewfinder/viewfinderStore.js';
import { useBlockPositionStore } from './blockPositionStore.js';

export const useBlockStore = defineStore('BlockStore', () => {
  // state
  const localBlocks = ref([]);
  const serverBlocks = ref([]);
  const blocksTypes = [
    {
      label: 'texte',
      type: 'markdown',
      isCreationOpen: true,
      design: {
        width: 500,
        height: 400,
      },
    },
    {
      label: 'image',
      type: 'image',
      isCreationOpen: false,
      design: {
        width: 450,
        height: 200,
      },
    },
  ];

  // stores
  const { currentSpaceName } = storeToRefs(useIndexStore());
  const { isViewfinderActive } = storeToRefs(useViewfinderStore());
  const {
    getRightMostBlock,
    getLowestBlock,
    getBlockRightEdgeX,
    getBlockBottomEdgeY,
    parseTransformForBlockCoordinates,
  } = useBlockPositionStore();

  // watchers
  watch(localBlocks, () => {
    localBlocks.value.forEach((localBlock) => {
      localBlock.isHighlight = localBlock.isHighlight ?? false;
      localBlock.isVisible = localBlock.isVisible ?? true;
      localBlock.isSelected = localBlock.isSelected ?? false;
      localBlock.isFlashing = localBlock.isFlashing ?? false;
      localBlock.isEdited = localBlock.isEdited ?? false;

      if (
        localBlock.content.originalContent ||
        localBlock.type === 'representative'
      )
        return;
      if (localBlock.type === 'markdown')
        localBlock.content.originalContent = localBlock.content.text;
      if (localBlock.type === 'code')
        localBlock.content.originalContent = localBlock.content.code;
      if (localBlock.type === 'image')
        localBlock.content.originalContent =
          localBlock.content.image[0] ?? null;
    });
    backupBlocks();
  });
  watch(serverBlocks, () => {
    serverBlocks.value.forEach((serverBlock) => {
      serverBlock.isHighlight = false;
      serverBlock.isVisible = true;
      serverBlock.isSelected = false;
      serverBlock.isFlashing = false;
      serverBlock.isEdited = false;

      if (
        serverBlock.content.originalContent ||
        serverBlock.type === 'representative'
      )
        return;
      if (serverBlock.type === 'markdown')
        serverBlock.content.originalContent = serverBlock.content.text;
      if (serverBlock.type === 'code')
        serverBlock.content.originalContent = serverBlock.content.code;
      if (serverBlock.type === 'image')
        if (serverBlock.content.image) {
          serverBlock.content.originalContent = serverBlock.content.image[0];
        }
    });
    backupBlocks();
  });

  // computed
  const isEditingABlock = computed(() =>
    localBlocks.value.some((block) => block.isEdited)
  );
  const highestBlock = computed(() => {
    const highestZIndexBlock = localBlocks.value.reduce(
      (prevBlock, currentBlock) => {
        if (
          currentBlock.content &&
          currentBlock.content.zindex > prevBlock.content.zindex
        ) {
          return currentBlock;
        }
        return prevBlock;
      }
    );
    return highestZIndexBlock || false;
  });
  const lowestBlock = computed(() => {
    const lowestZIndexBlock = localBlocks.value.reduce(
      (prevBlock, currentBlock) => {
        if (
          currentBlock.content &&
          currentBlock.content.zindex < prevBlock.content.zindex
        ) {
          return currentBlock;
        }
        return prevBlock;
      }
    );
    return lowestZIndexBlock || false;
  });

  const selectedBlocks = computed(() =>
    localBlocks.value.filter((block) => block.isSelected)
  );

  const selectedBlock = computed(() =>
    selectedBlocks.value.length === 1 ? selectedBlocks.value[0] : false
  );

  // static blocks are used if view is up to date (saveStore.js)
  const localStaticBlocks = computed(() => {
    const localStaticBlocks = localBlocks.value.map((localBlock) => {
      const localStaticBlock = Object.assign({}, localBlock);
      delete localStaticBlock.isHighlight;
      delete localStaticBlock.isVisible;
      delete localStaticBlock.isSelected;
      delete localStaticBlock.isFlashing;
      delete localStaticBlock.isEdited;

      return localStaticBlock;
    });
    return localStaticBlocks;
  });
  const serverStaticBlocks = computed(() => {
    const serverStaticBlocks = serverBlocks.value.map((serverBlock) => {
      const serverStaticBlock = Object.assign({}, serverBlock);
      delete serverStaticBlock.isHighlight;
      delete serverStaticBlock.isVisible;
      delete serverStaticBlock.isSelected;
      delete serverStaticBlock.isFlashing;
      delete serverStaticBlock.isEdited;

      return serverStaticBlock;
    });
    return serverStaticBlocks;
  });

  // methods
  function createBlock({
    type,
    id = Date.now(),
    refs = '',
    left = '0',
    top = '0',
    text = '',
  }) {
    backupBlocks();
    let newBlock = {
      type: type,
      isNew: true,
      isEdited: true,
      isHighlight: false,
      isLocal: true,
      id: id,
      content: {
        width:
          blocksTypes.find((blockType) => blockType.type === type).design
            .width + 'px',
        height:
          blocksTypes.find((blockType) => blockType.type === type).design
            .height + 'px',
        transform: `translate(${left}px, ${top}px)`,
        zindex: highestBlock.value.content.zindex + 1,
      },
    };

    if (type === 'markdown') {
      newBlock.content.text = text;
    }
    if (type === 'code') {
      newBlock.content.code = text;
    }
    if (type === 'image') {
      newBlock.content.image = [null];
      newBlock.content.thumb = '';
      newBlock.content.caption = '';
    }

    localBlocks.value.push(newBlock);
    focusField();
  }

  function removeSelectedBlocks() {
    localBlocks.value = localBlocks.value.filter(
      (localBlock) => !localBlock.isSelected
    );
  }

  function focusField() {
    setTimeout(() => {
      const field = document.querySelector('textarea');
      if (field) {
        field.focus();
      }
    }, 0);
  }

  function removeLastBlockIfEmpty() {
    // const lastBlock = getLastBlock()
    // if (lastBlock.type === "markdown" || lastBlock.type === "text") {
    //   if (
    //     lastBlock.content.text.length === 0 &&
    //     !e.target.classList.contains("keep-new-block")
    //   ) {
    //     this.store.removeBlockById(lastBlock.id)
    //   }
    // }
  }

  function removeBlockById(id) {
    localBlocks.value = localBlocks.value.filter((block) => block.id != id);
  }

  function editBlockById(id) {
    localBlocks.value = localBlocks.value.map((block) => {
      if (block.id == id) {
        block.isEdited = true;
      }
      return block;
    });
  }

  function getBlockById(id) {
    return localBlocks.value.find((block) => block.id == id);
  }

  function duplicateBlock(block) {
    const copy = JSON.parse(JSON.stringify(block));
    copy.id = String(Date.now());
    copy.isNew = true;
    copy.content.zindex = block.content.zindex + 1;

    const newX = parseInt(copy.content.transform.match(/[0-9]+/)[0]) + 15;
    const newY = parseInt(copy.content.transform.match(/[0-9]+/g)[1]) + 15;

    copy.content.transform = copy.content.transform.replace(/[0-9]+/g, newX);
    let t = 0;
    copy.content.transform = copy.content.transform.replace(
      /[0-9]+/g,
      (match) => (++t === 2 ? newY : match)
    );

    localBlocks.value.push(copy);
  }

  function duplicateSelectedBlocks() {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.isSelected) {
        duplicateBlock(localBlock);
      }
    });
  }

  function duplicateBlockById(id) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == id) {
        duplicateBlock(localBlock);
      }
    });
  }
  function duplicateSelectedBlocks() {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.isSelected) {
        const copy = JSON.parse(JSON.stringify(localBlock));
        copy.id = String(Date.now());
        copy.isNew = true;
        copy.content.zindex = localBlock.content.zindex + 1;

        const newX = parseInt(copy.content.transform.match(/[0-9]+/)[0]) + 15;
        const newY = parseInt(copy.content.transform.match(/[0-9]+/g)[1]) + 15;

        copy.content.transform = copy.content.transform.replace(
          /[0-9]+/g,
          newX
        );
        let t = 0;
        copy.content.transform = copy.content.transform.replace(
          /[0-9]+/g,
          (match) => (++t === 2 ? newY : match)
        );

        localBlocks.value.push(copy);
      }
    });
  }

  function selectAllBlocks() {
    localBlocks.value = localBlocks.value.map((block) => ({
      ...block,
      isSelected: true,
    }));
  }

  function selectBlockById(id) {
    unselectBlocks();
    localBlocks.value = localBlocks.value.map((block) => {
      if (block.id === id) {
        return {
          ...block,
          isSelected: true,
        };
      } else {
        return block;
      }
    });
  }

  function highlightBlockById(id) {
    localBlocks.value = localBlocks.value.map((block) => {
      if (block.id === id) {
        return {
          ...block,
          isHighlight: true,
        };
      } else {
        return block;
      }
    });
  }

  function unselectBlocks(event) {
    return new Promise((resolve) => {
      if (event?.target.classList.contains('prevent-unselect')) {
        resolve();
        return;
      }
      localBlocks.value = localBlocks.value.map((block) => ({
        ...block,
        isSelected: false,
        isEdited: false,
      }));
      resolve();
    });
  }

  function backupBlocks() {
    let backups = JSON.parse(localStorage.getItem('backups')) ?? [];
    const spaceBackupExist = backups.hasOwnProperty(currentSpaceName.value);

    if (!spaceBackupExist) backups[currentSpaceName.value] = [];
    if (backups[currentSpaceName.value].length === 20) {
      backups[currentSpaceName.value].pop();
    }
    backups[currentSpaceName.value].push(localBlocks.value);
    localStorage.setItem('backups', JSON.stringify(backups));
  }

  function upper(targetId) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == targetId) {
        localBlock.content.zindex += 1;
      }
    });
  }

  function lower(targetId) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == targetId) {
        localBlock.content.zindex =
          localBlock.content.zindex > 0 ? localBlock.content.zindex - 1 : 0;
      }
    });
  }

  function upperMax(targetId) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == targetId) {
        localBlock.content.zindex = highestBlock.value.content.zindex + 1;
      }
    });
  }

  function setAsIntroById(targetId) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == targetId) {
        localBlock.content.isintro = 'true';
      }
    });
  }

  function unsetAsIntroById(targetId) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == targetId) {
        localBlock.content.isintro = 'false';
      }
    });
  }

  function lowerMax(targetId) {
    localBlocks.value.forEach((localBlock) => {
      if (localBlock.id == targetId) {
        localBlock.content.zindex =
          lowestBlock.value.content.zindex > 0
            ? lowestBlock.value.content.zindex - 1
            : 0;
      }
    });
  }

  function getBlockIndex(block) {
    return localBlocks.value.indexOf(
      localBlocks.value.find((localBlock) => localBlock.id === block.id)
    );
  }

  function setBlockIndex(block, newIndex) {
    const currentIndex = getBlockIndex(block);

    if (currentIndex === -1) {
      console.log('setBlockIndex() : no corresponding block foudn for ', block);
      return;
    }

    const blockToMove = localBlocks.value.splice(currentIndex, 1)[0];

    localBlocks.value.splice(newIndex, 0, blockToMove);
  }

  function cancelLocalModifications() {
    if (window.confirm('Annuler les modifications non enregistrées ?'))
      localBlocks.value = JSON.parse(JSON.stringify(serverBlocks.value));
  }

  return {
    localBlocks,
    localStaticBlocks,
    serverBlocks,
    serverStaticBlocks,
    blocksTypes,
    highestBlock,
    lowestBlock,
    selectedBlocks,
    selectedBlock,
    isEditingABlock,
    selectBlockById,
    highlightBlockById,
    createBlock,
    getRightMostBlock,
    getLowestBlock,
    getBlockRightEdgeX,
    getBlockBottomEdgeY,
    getBlockById,
    editBlockById,
    duplicateBlockById,
    parseTransformForBlockCoordinates,
    removeLastBlockIfEmpty,
    duplicateSelectedBlocks,
    removeSelectedBlocks,
    removeBlockById,
    selectAllBlocks,
    unselectBlocks,
    backupBlocks,
    upper,
    upperMax,
    lower,
    lowerMax,
    setAsIntroById,
    unsetAsIntroById,
    getBlockIndex,
    setBlockIndex,
    cancelLocalModifications,
  };
});
