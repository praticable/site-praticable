import { defineStore } from "pinia"

export const usePolyfillsStore = defineStore("polyFillsStore", () => {
  function setPolyfills() {
    if (!String.prototype.matchAll) {
      String.prototype.matchAll = function (rx) {
        if (typeof rx === "string") rx = new RegExp(rx, "g") // coerce a string to be a global regex
        rx = new RegExp(rx) // Clone the regex so we don't update the last index on the regex they pass us
        let cap = [] // the single capture
        let all = [] // all the captures (return this)
        while ((cap = rx.exec(this)) !== null) all.push(cap) // execute and add
        return all // profit!
      }
    }
    if (!String.prototype.replaceAll) {
      String.prototype.replaceAll = function (str, newStr) {
        // If a regex pattern
        if (
          Object.prototype.toString.call(str).toLowerCase() ===
          "[object regexp]"
        ) {
          return this.replace(str, newStr)
        }

        // If a string
        return this.replace(new RegExp(str, "g"), newStr)
      }
    }
  }

  return { setPolyfills }
})
