# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

# Praticable

## Development

On Unix OS (macOS or Linux), you can use the `npm run dev` shortcut command to launch in parallel the PHP and Node servers. Go to http://localhost:8888 to see the project.

## Build

**Remove the `/public/media` folder before triggering the `npm run build` command.**
It contains symlinks that are not supported by the build process.

### Viewfinder

Every viewfinder navigations (index, tune, modify) must have a store containing at least its `isOpen` state so that the viewfinderStore can know if one of the navigation is open.
